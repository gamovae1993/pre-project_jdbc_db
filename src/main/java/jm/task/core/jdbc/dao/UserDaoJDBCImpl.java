package jm.task.core.jdbc.dao;

import jm.task.core.jdbc.model.User;
import jm.task.core.jdbc.util.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDaoJDBCImpl implements UserDao {
    static final String CREATE_DEVELOPERS_TABLE = """
            CREATE TABLE if not exists developers
            (
                id       serial primary key,
                name     varchar(50),
                lastName varchar(50),
                age      int4
            )
            """;

    static final String DROP_DEVELOPERS_TABLE = """
            DROP TABLE IF EXISTS developers;
                """;

    static final String SAVE_USER = """
            INSERT INTO developers (name, lastName, age) VALUES (?,?,?)
             """;

    static final String REMOVE_USER_BY_ID = """
            DELETE FROM developers WHERE id = ?;
            """;

    static final String GET_ALL_USERS = """
            SELECT * FROM developers
            """;

    static final String CLEAN_USERS_TABLE = """
            TRUNCATE TABLE developers CASCADE
            """;

    public UserDaoJDBCImpl() {

    }

    public void createUsersTable() {
        Connection connection = Util.getConnection();
        if (connection != null) {
            System.out.println("Connected to the database!");
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(CREATE_DEVELOPERS_TABLE);
                System.out.println("Table \"developers\" has been created");

                Util.closeConnection();
                System.out.println("Disconnecting from the database!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Error connecting to the database!");
        }
    }

    public void dropUsersTable() {
        Connection connection = Util.getConnection();
        if (connection != null) {
            System.out.println("Connected to the database!");
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(DROP_DEVELOPERS_TABLE);
                System.out.println("Table \"developers\" has been deleted");

                Util.closeConnection();
                System.out.println("Disconnecting from the database!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Error connecting to the database!");
        }
    }

    public void saveUser(String name, String lastName, byte age) {
        Connection connection = Util.getConnection();
        if (connection != null) {
            System.out.println("Connected to the database!");
            try (PreparedStatement preparedStatement = connection.prepareStatement(SAVE_USER)) {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, lastName);
                preparedStatement.setInt(3, age);
                preparedStatement.executeUpdate();
                System.out.println("User " + name + " has been added to the \"developers\" table ");

                Util.closeConnection();
                System.out.println("Disconnecting from the database!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Error connecting to the database!");
        }
    }

    public void removeUserById(long id) {
        Connection connection = Util.getConnection();
        if (connection != null) {
            System.out.println("Connected to the database!");
            try (PreparedStatement preparedStatement = connection.prepareStatement(REMOVE_USER_BY_ID)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                System.out.println("A user has been removed from the \"developers\" table");

                Util.closeConnection();
                System.out.println("Disconnecting from the database!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Error connecting to the database!");
        }
    }

    public List<User> getAllUsers() {
        Connection connection = Util.getConnection();
        List<User> users = new ArrayList<>();
        if (connection != null) {
            System.out.println("Connected to the database!");
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(GET_ALL_USERS);
                while (resultSet.next()) {
                    long id = resultSet.getLong("id");
                    String firstName = resultSet.getString("name");
                    String lastName = resultSet.getString("lastName");
                    byte age = resultSet.getByte("age");

                    User user = new User(id, firstName, lastName, age);
                    users.add(user);
                    Util.closeConnection();
                    System.out.println("Disconnecting from the database!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return users;
    }

    public void cleanUsersTable() {
        Connection connection = Util.getConnection();
        if (connection != null) {
            System.out.println("Connected to the database!");
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(CLEAN_USERS_TABLE);
                System.out.println("The table has been cleared");

                Util.closeConnection();
                System.out.println("Disconnecting from the database!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
