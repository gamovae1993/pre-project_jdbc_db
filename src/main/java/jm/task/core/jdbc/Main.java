package jm.task.core.jdbc;

import jm.task.core.jdbc.model.User;
import jm.task.core.jdbc.service.UserService;
import jm.task.core.jdbc.service.UserServiceImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        // реализуйте алгоритм здесь

        UserService userService = new UserServiceImpl();

        //Create a "developers" table
//        userService.createUsersTable();

        //Drop a "developers" table
//        userService.dropUsersTable();

        //Add a user to the "developers" table
        User user1 = new User("Roman", "Borgoyakov", (byte) 32);
//        User user2 = new User("MiniRoman", "Borgoyakov", (byte) 11);
//        User user3 = new User("MaxiRoman", "Borgoyakov", (byte) 43);
//        User user4 = new User("RomaRomaOLaLa", "Borgoyakov", (byte) 2);
//
        userService.saveUser(user1.getName(), user1.getLastName(), user1.getAge());
//        userService.saveUser(user2.getName(), user2.getLastName(), user2.getAge());
//        userService.saveUser(user3.getName(), user3.getLastName(), user3.getAge());
//        userService.saveUser(user4.getName(), user4.getLastName(), user4.getAge());

        //Remove User By ID
//        userService.removeUserById(44);

        //Get all users the table "developers";
//        List<User> users = userService.getAllUsers();
//        for (User user : users) {
//            System.out.println(user);
//        }

        //Clear the "developers" table
//        userService.cleanUsersTable();
    }
}
