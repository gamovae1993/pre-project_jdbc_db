package jm.task.core.jdbc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Util {
    // реализуйте настройку соеденения с БД

    private static final String PROPERTIES_FILE = "C:\\Java\\learning\\Kata Academy\\pre-project_jdbc_db_v2\\src\\main\\resources\\db.properties";
    private static Connection connection;

    public static Connection getConnection() {
        if (connection == null) {
            try (InputStream inputStream = new FileInputStream(new File(PROPERTIES_FILE))) {
                Properties properties = new Properties();
                properties.load(inputStream);

                String url = properties.getProperty("url");
                String userName = properties.getProperty("username");
                String password = properties.getProperty("password");

                connection = DriverManager.getConnection(url, userName, password);
            } catch (IOException | SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    public static void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
